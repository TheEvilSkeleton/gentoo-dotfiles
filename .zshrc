# Cosmetic
## Theme
#ZSH_THEME="powerlevel10k/powerlevel10k"
source ~/powerlevel10k/powerlevel10k.zsh-theme
## Powerlevel10k Configuration
POWERLEVEL9K_DISABLE_RPROMPT="true"
POWERLEVEL9K_PROMPT_ON_NEWLINE="true"
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=()
POWERLEVEL9K_MODE="nerdfont-complete"
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX=""
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(custom_gentoo_icon dir vcs)
POWERLEVEL9K_CUSTOM_GENTOO_ICON="echo "
POWERLEVEL9K_CUSTOM_GENTOO_ICON_BACKGROUND='magenta'
POWERLEVEL9K_DIR_HOME_BACKGROUND="13"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND="13"
## Other
neofetch

# Aliases
## Default
alias 'reboot'='sudo reboot'
alias 'sensors'='watch -n2 sensors'
alias 'sudoku'='sudo su'
alias 'java'='java -jar'
alias 'man'='man --html=firefox'
alias 'make'='make --jobs=12 --load-average=12'
alias 'sudo'='sudo -E'
alias 'ls'='ls -a'
## Gentoo
alias 'emerge'='sudo emerge'
alias 'em'='sudo emerge'
alias 'emerge-upgrade-all'='sudo emerge -vuUDN @world && sudo smart-live-rebuild'
alias 'em-upgrade-all'='sudo emerge -vuUDN @world && sudo smart-live-rebuild'
alias 'em-upgrade'='sudo emerge -vuUDN @world'
alias 'emerge-upgrade'='sudo emerge -vuUDN @world'
alias 'dispatch-conf'='sudo dispatch-conf'
alias 'emerge-sync'='sudo eix-sync'
alias 'em-sync'='sudo eix-sync'
alias 'eix-sync'='sudo eix-sync'
alias 'emerge-build-time'='sudo genlop -t'
alias 'em-build-time'='sudo genlop -t'
alias 'layman'='sudo layman'
alias 'send'='sudo emerge'
alias 'eselect'='sudo eselect'
alias 'smart-live-rebuild'='sudo smart-live-rebuild'
alias 'genlop'='sudo genlop'
alias 'emaint'='sudo emaint'

# Completion
autoload -Uz compinit promptinit
compinit
promptinit
zstyle ':completion:*' menu select

typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"      beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"       end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"    overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}" backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"    delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"        up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"      down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"      backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"     forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"    beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"  end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}" reverse-menu-complete

if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search

key[Control-Left]="${terminfo[kLFT5]}"
key[Control-Right]="${terminfo[kRIT5]}"

[[ -n "${key[Control-Left]}"  ]] && bindkey -- "${key[Control-Left]}"  backward-word
[[ -n "${key[Control-Right]}" ]] && bindkey -- "${key[Control-Right]}" forward-word

zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'

export HISTSIZE=2000
export HISTFILE="$HOME/.history"
export SAVEHIST=$HISTSIZE
setopt hist_ignore_all_dups
setopt autocd
setopt extendedglob

autoload -Uz select-word-style
select-word-style bash

# ix
ix () { curl -n -F 'f:1=<-' http://ix.io ; }
